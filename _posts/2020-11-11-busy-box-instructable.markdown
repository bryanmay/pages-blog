---
layout: post
title:  "DIY Busy Box"
date:   2020-11-11 16:25:14 -0500
categories: how-to
---

# DIY Busy Box

I wanted to *make* a gift for my 1 year old son rather than buying him something for Christmas this year. And he loves exploring things and figuring out what they do. So my wife suggested making a busy box! I'll post the final product now to let you know if you should keep reading 

<img src="{{site.baseurl}}/assets/final-lights.jpg" class="center">

Selfishly I also wanted to get more familiar on how to use GitLab pages to deploy a blog at no cost (yes I'm a nerd). I will write mostly about the former, and maybe create a second post for the latter. 

The inspiration for this came from my wife and some googling, particularly the projects below: 
- [LED Light Switch Busy Box](https://www.instructables.com/Childs-Toy-Light-Switch-Box/)
- [Reddit DIY Busy Box](https://www.reddit.com/r/beyondthebump/comments/9vk9ay/diy_busyboard_we_made/)
- [Reddit DIY busy Box 2](https://www.reddit.com/r/DIY/comments/7g8xy8/diy_toddler_busy_board_w_leds/)

## Supplies
I bought supplies from 3 different stores, 
1. An online hobby electronic store called [Sparkfun](https://www.sparkfun.com/) for the resistors, LEDs, battery pack, solder, wires, and shrink wrap. I also grabbed a soldering iron from here. I love buying new tools. 


2. From Home Depot I got light switches, electrical boxes, latching hardware and wood to make the box. The frame is used 2x4s and the front is a piece of 3/4 inch pine plywood that was sold in 2x4 ft pieces which was nice since I could fit it in my car unlike the standard 4x8 ft full sheet of plywood. I also got some brushes and tack cloth for the stain and polyurethane step. 

3. AC Moore store for letters to personalize each busy box and some other busy activities to add to the front. 

I used stain, polyurethane and and paint that I had around the house already. Also, I used  nails, screws and other hardware I had laying around. 

## Tools
- soldering iron
- jigsaw
- sandpaper - 180 and 320 grit
- 0000 dry wool
- circular saw
- electric drill
- 2 and 6 ft. levels (as a surrogate for a table saw)
- clamps
- hammer
- phillips screwdriver
- hot glue gun

## 1. Building the box

#### Building the frame and setting the front
We started with the frame and the front of the box. I wanted to have the frame inset from the top to make it easier to pick up and move. We used a 3/4 in. inlay. The frame is simple 2x4s held together with 2.5 inch nails. Would have used screws but didn't have any long enough. 

<img src="{{site.baseurl}}/assets/frame.jpg" class="center">

The front is held to the frame with 2 inch drywall screws that I will cover with things to make it look more finished. 

<img src="{{site.baseurl}}/assets/front.jpg" class="center">

After the boxes were put together, I positioned the 4 switch boxes on the face and traced the lines to cut. We started the cut on each box with a drilled hole, then cut the boxes with a jigsaw. The old work boxes can go anywhere on the face since it just pinches the front of the box to the back, eliminating the need to have a stud to drive into. 

<img src="{{site.baseurl}}/assets/jigsaw.jpg" class="center">

<img src="{{site.baseurl}}/assets/jigsaw_action.jpg" class="center">

## 2. Paint, Stain & Poly
Staining and polyurethaning the board was pretty straight forward. First I sanded the surface and edges with the 180 grit sandpaper and followed it by a pass with the 320 grit. Then, 2 rounds of Minwax stain and 3 coats of fast-drying polyurethane took a weekend (fast-dry is relative I guess - 4 hours in between each coat). I used 0000 steel wool in between each poly coat to remove bumps. 

<img src="{{site.baseurl}}/assets/drying-poly.jpg" class="center">


## 3. Adding Electrical 
I attached 2 ft. wires to each of the light switches and mounted the switches on the electrical box. At this point I put the switch covers on because all the remaining circuit work will be done behind the face of the board. 

<img src="{{site.baseurl}}/assets/back-wiring.jpg" class="center">

I mounted the LEDs by drilling a hole that was wider than the cathode/anode but smaller than the base of the LED. I used hot glue to keep the LED in place. 

<img src="{{site.baseurl}}/assets/led-mount.jpg" class="center">

Next, I needed to determine what resistors to use. In the [first reference](https://www.instructables.com/Childs-Toy-Light-Switch-Box/) I listed, the 4 LEDs were in parallel to the source and each one was the same resistance. I decided to modify that approach by having 2 LEDs controlled by a single switch, and another LED controlled by a 3 way switch. The circuit diagram changed slightly to account for the increased current needed for the 2 LEDs in parallel in the middle shown below. I used [this reference](https://www.evilmadscientist.com/2012/resistors-for-leds/) to figure out the amount of resistance I needed for the 2 AA battery source to power each LED.  

<img src="{{site.baseurl}}/assets/circuit-diagram.jpg" class="center">

Soldering the wires, LEDs, and resisters together was a stroll down memory lane since I took circuits classes as part of my degree in Computer Engineering. I just wrapped the wires to the ends of the LEDs/resistors and smothered it with solder to ensure the connection stayed secure. I used electrical tape to protect all the connections. I bought shrink wrap, but didn't have a strong enough heat gun to get it to shrink properly. Just be sure to use enough to keep the anode and cathode wires from touching!

<img src="{{site.baseurl}}/assets/resistor.jpg" class="center">

<img src="{{site.baseurl}}/assets/back-wiring2.jpg" class="center">

## 4. Doors, Hinges, Locks and more

First we tried to lay everything out to ensure we had enough space without it getting crowded. And in the second box, we actually excluded a set of doors to allow for more space. In hindsight, we should have done this before putting the switch boxes in. 

<img src="{{site.baseurl}}/assets/initial_layout.png" class="center">

We made two sets of doors for each of the busy boxes out of old 1/4 in. wainscoting plywood. It was a pain to do without a table saw. We had to measure an extra 1 1/2 in to put a level as a guide for a circular saw. A few times, this is fine, but we had about 2 dozen cuts for all 8 doors, which took forever. 

<img src="{{site.baseurl}}/assets/level-guide.jpg" class="center">

Once the doors we're cut, we added the hinge hardware which needed to be done with a very small phillips head bit. 

<img src="{{site.baseurl}}/assets/hinges.jpg" class="center">

We realized that with the types of hinges we had, the doors needed a frame to be mounted to to ensure they didn't get blocked after 90 degrees. So we had to cut and stain/poly these and used a 18 gauge nail gun to attach them to the board. Would recommend getting cabinet hinges to remove this annoying step. 

<img src="{{site.baseurl}}/assets/poly-frame.jpg" class="center">

Attaching the hardware was pretty straightforward for most of it, just measure from one side to make sure its straight. The eye hooks that were used to mimic shoelaces were the only ones that needed to measure spacing. I used a hex bit and a crescent wrench to get them in after spacing them out. 

<img src="{{site.baseurl}}/assets/eye-hooks.jpg" class="center">

## 5. Decoration 

We found some letters and other wooden decorations that we painted with acrylic paint from AC Moore. 

<img src="{{site.baseurl}}/assets/painting-letters.jpg" class="center">

We used a hot glue gun to attach all the decorative wooden pieces. 

<img src="{{site.baseurl}}/assets/ro-final.jpg" class="center">

<img src="{{site.baseurl}}/assets/final-top-down.jpg" class="center">

## Results

Both the kids loved them! The light switches and door stop springs were the initial favorites. Would definitely recommend something like this for toddlers between 1 and 3 years old. 

<img src="{{site.baseurl}}/assets/reveal.jpg" class="center">

<img src="{{site.baseurl}}/assets/reveal2.jpg" class="center">

<img src="{{site.baseurl}}/assets/reveal3.jpg" class="center">

<img src="{{site.baseurl}}/assets/reveal4.jpg" class="center">

#### Time and Money Spent

This took a total of probably 20 hours to make 2 of them. The initial box builds + door cut-outs took about 3 hours. The decoration, painting and poly took a total of about 4 hours across a few weekend days and week nights. The wiring was about 2 hours for the first box, and 1 hour for the second. And the time to attach all the hardware was about an 2 hours each. The additional time was little stuff like going to the store to get supplies, printing photos to put behind the doors, etc. It adds up. 

Without the new tools I had to buy (soldering iron, jigsaw), the boards were about $100 each, most of which coming from the wood and hardware. Subsequent boards will be less expensive since I had to buy some items in bulk (e.g. resistors). 
